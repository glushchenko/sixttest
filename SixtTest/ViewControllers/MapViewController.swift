//
//  MapViewController.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var cars: [Car] = [] {
        didSet {
            if self.isViewLoaded {
                self.loadAnnotations()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadAnnotations()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadAnnotations() {
        let annotations: [MKAnnotation] = cars.map { (car) -> CarAnnotation in
            return CarAnnotation(with: car)
        }
        self.mapView.addAnnotations(annotations)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? CarAnnotation else { return nil }
        
        var annotationView: MKAnnotationView
        if let view = mapView.dequeueReusableAnnotationView(withIdentifier: CarAnnotationView.ID) {
            view.annotation = annotation
            annotationView = view
        }
        else {
            let view = CarAnnotationView(annotation: annotation, reuseIdentifier: CarAnnotationView.ID)
            annotationView = view
        }
        
        if let button = annotationView.rightCalloutAccessoryView as? UIButton {
            if let car = annotation.car {
                let url = URL(string: car.carImageUrl)
                let placeholder = UIImage(named: "car-placeholder")
                button.kf.setImage(with: url, for: .normal, placeholder: placeholder, options: nil, progressBlock: nil) { (result) in
                    switch result {
                        case .success(_): break
                        case .failure(_): button.setImage(placeholder, for: .normal)
                    }
                }
            }
            else {
                button.setImage(nil, for: .normal)
            }
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation as? CarAnnotation else { return }
        let alert = UIAlertController.ask(.openAtMap, text: nil) { () in
            annotation.mapItem().openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        }
        self.present(alert, animated: true, completion: nil)
    }
}

private extension String {
    static let openAtMap = NSLocalizedString("Would you like open this point at Maps?", comment: "")
}
