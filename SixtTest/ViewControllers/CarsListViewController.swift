//
//  CarsListViewController.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit
import Kingfisher

class CarsListViewController: UIViewController {

    let showMapSegue = "showMapSegue"
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl: UIRefreshControl?
    
    var cars: [Car] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refresh = UIRefreshControl()
        self.tableView.addSubview(refresh)
        refresh.addTarget(self, action: #selector(endRefreshing(sender:)), for: .valueChanged)
        self.refreshControl = refresh
        
        self.tableView.register(UINib(nibName: CarTableViewCell.ID, bundle: nil), forCellReuseIdentifier: CarTableViewCell.ID)
        
        self.reloadCars()
    }
    
    func reloadCars() {
        Car.retriveAll { [weak self] (cars, error) in
            self?.refreshControl?.endRefreshing()
            if let error = error {
                let alert = UIAlertController.show(error, completionHandler: { [weak self] in
                    self?.reloadCars()
                })
                self?.present(alert, animated: true, completion: nil)
            }
            else {
                self?.cars = cars ?? []
            }
        }
    }

    @objc func endRefreshing(sender: UIRefreshControl) {
        self.reloadCars()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.showMapSegue,
            let vc = segue.destination as? MapViewController {
            vc.cars = self.cars
        }
    }
}

extension CarsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CarTableViewCell.ID, for: indexPath) as? CarTableViewCell else { return UITableViewCell() }
        let car = self.cars[indexPath.row]
        
        cell.viewModel = CarTableViewCell.ViewModel(car: car)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cars.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
