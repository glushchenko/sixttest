//
//  Car.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit

class Car: NSObject {

    enum TransmissionType {
        case manual
        case automatic
        case unknowed
    }
    
    var modelIdentifier = ""
    var series = ""
    var name = ""
    var fuelType = ""
    var licensePlate = ""
    var transmission = TransmissionType.unknowed
    var color = ""
    var id = ""
    var innerCleanliness = ""
    var make = ""
    var carImageUrl: String {
        get {
            return NetworkLayer.urlForCarImage(with: modelIdentifier, and: color)
        }
    }
    var group = ""
    var modelName = ""
    var latitude = 0.0
    var longitude = 0.0
    var fuelLevel = 0.0
    
    var transmissionType: String {
        get {
            if transmission == .manual {
                return "Manual"
            }
            else if transmission == .automatic {
                return "Automatic"
            }
            else {
                return "Other"
            }
        }
    }
    
    init(dictionary: [AnyHashable: Any]) {
        super.init()
        
        if let modelIdentifier = dictionary["modelIdentifier"] as? String {
            self.modelIdentifier = modelIdentifier
        }
        if let series = dictionary["series"] as? String {
            self.series = series
        }
        if let name = dictionary["name"] as? String {
            self.name = name
        }
        if let fuelType = dictionary["fuelType"] as? String {
            self.fuelType = fuelType
        }
        if let licensePlate = dictionary["licensePlate"] as? String {
            self.licensePlate = licensePlate
        }
        if let transmission = dictionary["transmission"] as? String {
            self.transmission = self.transmissionToEnum(transmission)
        }
        if let color = dictionary["color"] as? String {
            self.color = color
        }
        if let id = dictionary["id"] as? String {
            self.id = id
        }
        if let innerCleanliness = dictionary["innerCleanliness"] as? String {
            self.innerCleanliness = innerCleanliness
        }
        if let make = dictionary["make"] as? String {
            self.make = make
        }
        if let group = dictionary["group"] as? String {
            self.group = group
        }
        if let modelName = dictionary["modelName"] as? String {
            self.modelName = modelName
        }
        if let latitude = dictionary["latitude"] as? Double {
            self.latitude = latitude
        }
        if let longitude = dictionary["longitude"] as? Double {
            self.longitude = longitude
        }
        if let fuelLevel = dictionary["fuelLevel"] as? Double {
            self.fuelLevel = fuelLevel
        }
    }
    
    class func retriveAll(completionHandler:@escaping ((_ cars: [Car]?, _ error: Error?) -> Void)) {
        NetworkLayer.retriveCars { (response, error) in
            guard let carsJson = response as? [[AnyHashable: Any]] else {
                completionHandler(nil, error)
                return
            }
            
            if let error = error {
                completionHandler(nil, error)
                return
            }
            
            let cars = carsJson.map({ (object) -> Car in
                return Car(dictionary: object)
            })
            
            completionHandler(cars, nil)
        }
    }
}

extension Car {
    fileprivate func transmissionToEnum(_ transmission: String) -> TransmissionType {
        if transmission == "M" {
            return .manual
        }
        else if transmission == "A" {
            return .automatic
        }
        else {
            return .unknowed
        }
    }
}
