//
//  NetworkLayer.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit
import Alamofire

class NetworkLayer: NSObject {

    static private let carsUrl = "http://www.codetalk.de/cars.json"
    static private let carImageUrlFormat = "https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/%@/%@/2x/car.png"
    
    class func retriveCars(queue: DispatchQueue = DispatchQueue.main, completionHandler:((_ response: Any?, _ error: Error?) -> Void)?) -> Void {
        AF.request(carsUrl).responseJSON { (response) in
            queue.async {
                completionHandler?(response.value, response.error)
            }
        }
    }
    
    class func urlForCarImage(with modelIdentifier: String, and color: String) -> String {
        return String(format: carImageUrlFormat, modelIdentifier, color)
    }
}
