//
//  UIAlertController+Additions.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit

extension UIAlertController {
    class func show( _ error: Error, completionHandler: (() -> Void)?) -> UIAlertController{
        let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Retry", style: .default) { (action) in
            completionHandler?()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    class func ask( _ title: String?, text: String?, okTitle: String? = "Yes", cancelTitle: String? = "Cancel", completionHandler: (() -> Void)?) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: okTitle, style: .default) { (action) in
            completionHandler?()
        }
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        return alert
    }
}
