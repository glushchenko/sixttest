//
//  CarAnnotation.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit
import MapKit

class CarAnnotation: NSObject, MKAnnotation {

    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    let car: Car?
    
    init(with car: Car) {
        title = car.modelName
        subtitle = String(format: "Transmission: %@, Fuel: %0.1f%%", car.transmissionType, car.fuelLevel * 100.0)
        coordinate = CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude)
        self.car = car
    }
    
    func mapItem() -> MKMapItem {
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        
        return mapItem
    }
}
