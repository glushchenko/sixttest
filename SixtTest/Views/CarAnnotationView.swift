//
//  CarAnnotationView.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit
import MapKit

class CarAnnotationView: MKAnnotationView {
    static let ID = "CarAnnotationView"
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.image = UIImage(named: "car_pin")
        self.canShowCallout = true
        self.calloutOffset = CGPoint(x: -5, y: 5)
        let button = UIButton(type: .custom)
        button.bounds.size.width = 40
        button.bounds.size.height = 40
        button.imageView?.contentMode = .scaleAspectFit
        self.rightCalloutAccessoryView = button
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
