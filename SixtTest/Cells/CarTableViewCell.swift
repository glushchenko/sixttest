//
//  CarTableViewCell.swift
//  SixtTest
//
//  Created by Sergey Glushchenko on 9/18/17.
//  Copyright © 2017 Esengy. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {

    struct ViewModel {
        var title: String {
            return car.modelName
        }
        var previewImageURL: URL? {
            return URL(string: car.carImageUrl)
        }
        var transmissionType: String {
            return car.transmissionType
        }
        var fuelType: String {
            return car.fuelType
        }
        var color: String {
            return car.color
        }
        
        let car: Car
    }
    
    static let ID = "CarTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var transmissionLabel: UILabel!
    @IBOutlet weak var fuelTypeLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    
    var viewModel: ViewModel? {
        didSet {
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI() {
        self.titleLabel.text = viewModel?.title
        self.fuelTypeLabel.text = viewModel?.fuelType
        self.transmissionLabel.text = viewModel?.transmissionType
        self.colorLabel.text = viewModel?.color
        
        let url = viewModel?.previewImageURL
        let placeholder = UIImage(named: "car-placeholder")
        self.previewImageView.kf.cancelDownloadTask()
        self.previewImageView.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil) { [weak self] (result) in
            switch result {
            case .success(_): break
            case .failure(_): self?.previewImageView.image = placeholder
            }
        }
    }
}
